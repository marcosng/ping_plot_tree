'''
PING-PLOT-TREE V2.4
===================
Esta rutina se encarga de leer un archivo Excel (en el cual está cargada la red de MMWW) y tirar ping a cada uno de los
NE's. Luego se imprime en consola el status del NE (Connected/No contact) según tenga gestion o no.
Finalizada la tarea de ping, se realiza un grafico de las cadenas de gestion, coloreando diferente cada sitio segun
el status (con/sin gestion). Los sitios origen de cadena de gestion, estarán representados con una forma geometrica diferente
al del resto.
'''

import os
import igraph
import openpyxl
import ipaddress
import subprocess
from igraph import *
from openpyxl import *
'''
 PARAMETROS
 ====================================================================
 archivo: nombre del archivo xlsx donde se encuentra cargada la red.
 hoja: nombre de la hoja del archivo xlsx donde se encuentran los datos de la red.
 archivo_nuevo: nombre del nuevo archivo xlsx a guardar

 column_IPA: numero de columna en la que se encuentra la direccion de IP del sitio A.
 column_IPB: numero de columna en la que se encuentra la direccion de IP del sitio B.
 colum_IDA: nunmero de la columna donde se encuentra el Cell ID del sitio A.
 colum_IDB: numero de la columna donde se encuentra el Cell ID del sitio B.
 colum_orig: columna con el Cell ID del sitio origen de la cadena.
 offset: offset de fila. Indicar cual es la primera fila con datos relevantes.
 
 
 color_OK: color del sitio que presenta todos sus equipos con gestion (Connected).
 color_NOOK: color de sitio que preseta al menos uno de sus equipos sin gestion (No contact).
 forma_origen: forma a representar del sitio origen de la cadena. (rectangle, circle, hidden, triangle-up, triangle-down)
 forma_sitio: forma a representar de los sitios. (rectangle, circle, hidden, triangle-up, triangle-down)
 etiq_sitio_tam: tamaño de la fuente de la etiqueta de los sitios
 etiq_link_tam: tamaño de la fuente de la etiqueta de los links
 sitio_tam: tamaño de la figura de los sitios
'''
archivo = "RedUy.xlsx"
hoja = "Hoja1"
archivo_nuevo = "StatusUy.xlsx"

colum_IPA = 45
colum_IPB = 47
colum_IDA = 2
colum_IDB = 3
colum_orig= 9
offset = 5

color_OK = "green"
color_NOOK = "yellow"
forma_origen = "rectangle"
forma_sitio = "circle"
etiq_sitio_tam = 10
etiq_link_tam = 10
sitio_tam = 38

'''
====================================================================
'''
# Inicializo el archivo Excel para leer/escribir
# ----------------------------------------------
file = openpyxl.load_workbook( archivo )
sheet = file.get_sheet_by_name( hoja )
#dato = sheet.cell(row = fila, column = columna).value              Para leer datos de una celda
#sheet.cell(row = fila, column = columna).value = "HOLA"            Para escribir datos en una celda



# Creo una lista con los sitios y los labels de los edges
# -------------------------------------------------------
link = []
sitios = []
for i in range(0, sheet.max_row - offset + 1 ):
    sitios.append( sheet.cell(row=i+offset, column=colum_IDA).value )
    sitios.append( sheet.cell(row=i+offset, column=colum_IDB).value )            
    link.append( str( sheet.cell(row=i+offset, column=colum_IDA).value) +"-"+ str( sheet.cell(row=i+offset, column=colum_IDB).value )  )

sitios = set(sitios)
sitios = list(sitios)
print("Links:", link)
print("\nSitios:", sitios)
print()



# Ocultar consola
# ---------------
info = subprocess.STARTUPINFO()
info.dwFlags |= subprocess.STARTF_USESHOWWINDOW
info.wShowWindow = subprocess.SW_HIDE



# Lectura de valor, conversion a IP, pingeo e impresion
# -----------------------------------------------------
notok = []
contador = int()

sheet.cell(row=offset-1, column=colum_IPA+7).value = "Status IP A"
sheet.cell(row=offset-1, column=colum_IPB+7).value = "Status IP B"

for j in range(0, sheet.max_row - offset+1):
    for i in [colum_IPA, colum_IPB]:
        ip = ipaddress.ip_address( str(sheet.cell(row=j+offset, column=i).value ) )
        output = subprocess.Popen(['ping','-n','1', '-w', '500', str(ip)], stdout=subprocess.PIPE, startupinfo=info).communicate()[0]
        if "recibidos = 1" in output.decode('ISO-8859-1'):
            print(ip, ": Connected")
            sheet.cell(row=j+offset, column=i+7).value = "Connected"
        else:
            print(ip, ": No contact")
            sheet.cell(row=j+offset, column=i+7).value = "No contact"
            if i == colum_IPA:
                notok.append( sheet.cell(row=j+offset, column=colum_IDA).value )
            if i == colum_IPB:
                notok.append( sheet.cell(row=j+offset, column=colum_IDB).value )
    contador = contador+1
    if contador == 100:                                         # Guardo el archivo Excel cada 100 filas por si acaso
        file.save(archivo_nuevo)
        contador = 0
        
file.save(archivo_nuevo)                                        # Guardo el archivo Excel al finalizar




g = Graph()
 # Creo los sitios
# ---------------
for i in range(0, len(sitios) ):
    g.add_vertex( sitios[i] )



# Creo los links
# --------------
for i in range(0, sheet.max_row - offset + 1):
    g.add_edge( sheet.cell(row=i+offset, column=colum_IDA).value, sheet.cell(row=i+offset, column=colum_IDB).value )



# Coloreo los "No contact"
# ------------------------
notok = set(notok)
notok = list(notok)
print("\nSitios sin gestion:", notok)
sitio_color = [color_OK]*len(sitios)                                    # Coloreo todos los sitios
for n in range (0, len(notok) ):                                        # Cambio el color a los sitios sin gestion
    sitio_color[ sitios.index( notok[n] ) ] = color_NOOK



# Sitios inicio de cadena
# ------------------------
origen = []
for k in range(0, sheet.max_row - offset+1):
    origen.append( sheet.cell(row=k+offset, column=colum_orig).value )
origen = set(origen)                                                    # Elimino duplicados
origen = list(origen)                                                   # Convierto origen a una lista
try:
    origen.pop( origen.index(None) )                                    # Elimino elemento None (vacio)
except:
    origen.pop( origen.index("") )

for k in range(0, len(origen) ):                                        # Extraigo Cell ID
    origen[k] = (origen[k][0:5] )
print("\nSitios origen de cadena:", origen)



# Cambio forma a triangulo en nodos
# ----------------------------------
forma = [forma_sitio]*len(sitios)                                       # Doy forma a todos los sitios
for i in range(0, len(origen)):                                         # Cambio forma de los sitios origen
    forma[ sitios.index( origen[i] )] = forma_origen



# Ploteo
# ------
plot(g, vertex_label = sitios,  vertex_label_size = etiq_sitio_tam,  vertex_size = sitio_tam,  vertex_color = sitio_color,  vertex_shape = forma,  edge_label = link,  edge_width = 3,  edge_label_size = etiq_link_tam )

